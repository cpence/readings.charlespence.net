---
title: Please sign in
---

# Please sign in

{{< rawhtml >}}

<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
  <div class="row">
    <div class="six columns">
      <label for="pass">Password</label>
      <input class="u-full-width" type="password" id="password" name="password" placeholder="Password" />
      <?php if ($error != "") { ?>
      <legend><?php echo $error; ?></legend>
      <?php } ?>
    </div>
  </div>
  <input class="button-primary" type="submit" value="Sign in">
</form>

{{< /rawhtml >}}
