---
title: "LSC1120A : Lecture"
---

# LSC1120A : Lecture

<a href="https://charlespence.net/fr/courses/lsc1120a/" class="button button-primary">Général</a>
<a href="https://charlespence.net/fr/courses/lsc1120a/agenda/" class="button button-primary">Agenda</a>
<a href="lsc1120a.php" class="button button-primary">Lecture</a>

-----

### Lecture obligatoire {.syllabus-day}

1. [Trois lettres sur la nucléaire](/readings/Lettres-Nucleaire.pdf)
2. [Girel, Mathias. 2013. « Agnotologie : mode
   d’emploi ».](/readings/Girel2013.pdf)
3. [Huneman, Philippe. 2023. « Signatures et prédiction ? Le tournant
   épistémique des données massives ».](/readings/Huneman2023.pdf)
4. [Mazauric, Simone. 2009. « Lavoisier et la révolution chimique ». Extrait de
   _Histoire des sciences à l’époque moderne._](/readings/Mazauric2009.pdf)
5. [Bonnet-Bidaud, Jean-Marc. 2013. « La science
   chinoise ».](/readings/BonnetBidaud2013.pdf)  
   *ou*  
   [Weinberg, Achille. 2013. « L’apport du monde
   arabe ».](/readings/Weinberg2013.pdf)
6. [Bourguet, Marie-Noëlle et Pierre-Yves Lacour. 2015. « Les mondes
   naturalistes : Europe (1530-1802) ». Extrait de _Histoire des sciences et des
   savoirs (I).](/readings/Bourguet2015.pdf)
<!--7. [Bravo, Felipe. 2017. « Platonisme -->
<!--mathématique ».](/readings/Bravo2017.pdf)-->
7. [Davies, Paul. 2007. « Comment visiter le futur ». Extrait de _Comment
   construire une machine à explorer le temps ?_](/readings/Davies2007.pdf)
8. [Barberousse, Anouk. 2008. « Les simulations numériques de l’évolution du
   climat : de nouveaux problèmes
   philosophiques ? ».](/readings/Barberousse2008.pdf)
9. [Debray, Stéphanie. 2020. « Pseudoscience ».](/readings/Debray2020.pdf)
10. [Laval, Christian, Francis Vergne, Pierre Clément, et Guy Dreux. 2011. « Le
    régime néolibéral de la connaissance ». Extrait de _La nouvelle école
    capitaliste._](/readings/Laval2011.pdf)
11. [Pestré, Dominique. 2006. « Femmes, genre et science : objectivité et parti
    pris ». Extrait de _Introduction aux_ Science
    Studies.](/readings/Pestre2006.pdf)
12. [Winner, Langdon. 2022 \[1980\]. « Les artefacts sont-ils politiques ? ».
    Extrait de _La baleine et la réacteur._](/readings/Winner2022.pdf)

### Autres ressources {.syllabus-day}

- Semaine 1
  - [Coutellec, Léo. 2013. « Une réévaluation du pluralisme
    axiologique ».](/readings/Coutellec2013.pdf)
- Semaine 2
  - [Oreskes, Naomi and Erik M. Conway. 2012. « Le déni du réchauffement
    climatique ». Extrait de _Les marchands de
    doute._](/readings/OreskesConway2012.pdf)
  - [Proctor, Robert. 2008. “Agnotology: A Missing Term to Describe the Cultural
    Production of Ignorance (and its Study).”](/readings/Proctor2008.pdf)
- Semaine 3
  - [Anderson, Chris. 2008. “The End of Theory: The Data Deluge Makes the
    Scientific Method Obsolete.”](/readings/Anderson2008.pdf)
  - [boyd, danah and Kate Crawford. 2012. “Critical Questions for Big
    Data.”](/readings/boyd2012.pdf)
- Semaine 4
  - [Bensaude-Vincent, Bernadette et Isabelle Stengers. 1993. _Histoire de la
    chimie_ (extrait).](/readings/BensaudeVincent1993extr.pdf)
  - [Kuhn, T. S. 1983. _La structure des révolutions scientifiques_
    (extraits).](/readings/Kuhn1983extr.pdf)
- Semaine 5
  - s.o.
- Semaine 6
  - [Cleland, Carol. 2001. “Historical Science, Experimental Science, and the
    Scientific Method.”](/readings/Cleland2001.pdf)
<!-- - Semaine 7
  - [Nahin, Paul. 1998. “The Puzzles of Imaginary Numbers.” Excerpt from _An
    Imaginary Tale: The Story of √-1._](/readings/Nahin1998.pdf) -->
- Semaine 7
  - [Gott, J. R. 2001. “Time Travel to the Past: Cosmic Strings.” From _Time
    Travel in Einstein’s Universe._](/readings/Gott2001.pdf)
  - [Deutsch, David and Michael Lockwood. 1994. “The Quantum Physics of Time
    Travel.”](/readings/Deutsch1994.pdf)
  - [Lewis, David. 1976. “The Paradoxes of Time
    Travel.”](/readings/Lewis1976.pdf)
- Semaine 8
  - [Varenne, Franck. 2012. « Modèle et réalité ». Extrait de _Théorie, réalité,
    modèle._](/readings/Varenne2012.pdf)
- Semaine 9
  - [Hansson, Sven Ove. 2021. “Science and
    Pseudo-Science.”](/readings/Hansson2021.pdf)
  - [Pigliucci, Massimo, 2015. “Scientism and Pseudoscience: A Philosophical
    Commentary.”](/readings/Pigliucci2015.pdf)
- Semaine 10
  - [Mirowski, Philip. 2012. “The Modern Commercialization of Science is a
    Passel of Ponzi Schemes.”](/readings/Mirowski2012.pdf)
  - [Mirowski, Philip and Robert van Horn. 2005. “The Contract Research
    Organization and the Commercialization of Scientific
    Research.”](/readings/Mirowski2005.pdf)
- Semaine 11
  - [Richardson, Sarah. 2008. “When Gender Criticism Becomes Standard Practice:
    The Case of Sex Determination Genetics.”](/readings/Richardson2008.pdf)
  - [Okruhlik, Kathleen. 1994. “Gender and the Biological
    Sciences.”](/readings/Okruhlik1994.pdf)
- Semaine 12
  - [Latour, Bruno. 2011. “Love Your Monsters: Why We Must Care for our
    Technologies as We Do Our Children.”](/readings/Latour2011.pdf)
  - [Lancelot, Mathilde. 2019. « Technologie ».](/readings/Lancelot2019.pdf)
