---
title: Course Readings
---

# Lecture du cours / Course Readings

Veuillez choisir votre cours de la liste ci-dessous.  
Select your course from the list below.

- [LFILO1220: Philosophie des sciences]({{<relref "lfilo1220.md">}})
- [LFILO2202: Éthique biomédicale]({{<relref "lfilo2202.md">}})
<!-- - [LFILO2602: Philosophie des sciences (questions approfondies) / Philosophy of
  Science (advanced studies)]({{<relref "lfilo2602.md">}}) -->
- [LSC 1120A: Notions de philosophie (partie générale)]({{<relref "lsc1120a.md"
  >}})
- [LSC 1120B: Notions de philosophie, d'éthologie, et d'éthique (partie
  VETE)]({{<relref "lsc1120b.md" >}})

{{< news >}}

## Note

Toute les lectures ici restent sous droit d'auteur de leurs auteurs/éditeurs
d'origine, et sont reproduites ici uniquement pour l'usage de mes étudiant·e·s.

---

All readings here remain copyright of their original publishers, and are
reproduced here only for the use of my students.

{{< /news >}}
