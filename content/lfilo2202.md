---
title: "LFILO2202 : Lecture"
---

# LFILO2202 : Lecture

<a href="https://charlespence.net/fr/courses/lfilo2202/" class="button button-primary">Général</a>
<a href="https://charlespence.net/fr/courses/lfilo2202/agenda" class="button button-primary">Agenda</a>
<a href="lfilo2202.php" class="button button-primary">Lecture</a>
<a href="https://charlespence.net/fr/courses/lfilo2202/travail" class="button button-primary">Travail</a>

--------------------------------------------------------------------------------

Toute de la lecture pour ce cours est ci-dessous.

En bas de la page, il y a aussi quelques autres resources qui pourraient être
utiles pour vos présentations et travaux finals.

--------------------------------------------------------------------------------

-   18 septembre :
    -   [Saint-Arnaud (1999), « Qu’est-ce que la
        bioéthique ? »](/readings/SaintArnaudCh1.pdf)
    -   [Keeling et Bellefleur (2016), « Le « principisme » et les cadres de
        référence en matière d’éthique en sante
        publique »](/readings/Keeling2016.pdf)
    -   (*facultatif*) [Benaroyo (2010), « L’émergence de la
        bioéthique »](/readings/Benaroyo2010.pdf)
    -   (*facultatif*) [Beauchamp and Childress (2013), “Moral Foundations”
        (ch. 1)](/readings/BeauchampCh1.pdf)
    -   (*facultatif*) [Tong (2002), “Teaching Bioethics in the New
        Millennium”](/readings/Tong2002.pdf)
-   25 septembre :
    -   [Wakefield (1992), « Le concept de trouble
        mental »](/readings/Wakefield1992fr.pdf) ([also available in
        English](/readings/Wakefield1992en.pdf))
    -   (*facultatif*) [Boorse (1977), « Le concept théorique de
        santé »](/readings/Boorse1977fr.pdf) ([also available in
        English](/readings/Boorse1977en.pdf))
    -   (*facultatif*) [Hesslow (1993), « Avons-nous besoin d’un concept de
        maladie ? »](/readings/Hesslow1993fr.pdf) ([also available in
        English](/readings/Hesslow1993en.pdf))
-   2 octobre :
    -   [Jaunait, « Comment peut-on être
        paternaliste ? »](/readings/Jaunait2003.pdf)
    -   [Constantine, « Divulgation de renseignements
        médicaux »](/readings/ConstantineDivulgation.pdf) ([also available in
        English](/readings/ConstantineDisclosure.pdf))
    -   [Callanan, « Dire la vérité au patient »](/readings/CallananVérité.pdf)
        ([also available in English](/readings/CallananTruth.pdf))
    -   [McRae, « Le signalement de patients pouvant causer préjudice à autrui
        (sécurité publique) »](/readings/McRaeSécurité.pdf) ([also available in
        English](/readings/McRaeSafety.pdf))
-   9 octobre :
    -   [Beauchamp et Childress (2007), « Le respect de
        l’autonomie »](/readings/BeauchampCh3fr.pdf) \[**p. 91-110 obligatoire,
        le reste facultatif**\] ([also available in
        English](/readings/BeauchampCh4.pdf) \[p. 101–114 required, remainder
        optional\])
    -   [Saint-Arnaud (1999), « L’euthanasie : des équivoques à
        dissiper »](/readings/SaintArnaudCh6.pdf)
    -   (*facultatif*) [Mortier (2003), « Directives anticipées : origines,
        utilité, fondements et controverses »](/readings/Mortier2003.pdf)
    -   [Charland et Lackmann, « Capacité de
        décision »](/readings/CharlandCapacité.pdf) ([also available in
        English](/readings/CharlandCapacity.pdf))
    -   [Sobsey et al., « L’avortement et la capacité à
        consentir »](/readings/SobseyAvortement.pdf) ([also available in
        English](/readings/SobseyAbortion.pdf))
    -   [Institut Européen de bioéthique, Rapport sur l’euthanasie en Belgique,
        2018-2019](/readings/Euthanasie2020.pdf)
-   16 octobre :
    -   [Moyse (2010), « Le concept de personne dans le champ du
        handicap »](/readings/Moyse2010.pdf)
    -   [Marin (2010), « La maladie chronique ou le temps
        douloureux »](/readings/Marin2010.pdf)
    -   (*facultatif mais recommandé*) [Fins (2023), « The complicated legacy of
        Terry Wallis and his brain injury »](/readings/Fins2023.pdf)
    -   (*facultatif*) [Terzi (2004), « The social model of disability: a
        philosophical critique »](/readings/Terzi2004.pdf)
        -   (*facultatif*) [Wendell (2001), « Unhealthy disabled: treating chronic
        illnesses as disabilities »](/readings/Wendell2001.pdf)
-   23 octobre :
    -   [Pelluchon (2009), « Justice distributive et
        solidarité »](/readings/Pelluchon2009.pdf) \[**p. 101-120 obligatoire,
        le reste facultatif**\]
    -   (*facultatif*) [de Kervasdoué (2010), « Économie et gestion de la
        santé : l’argent des autres »](/readings/deKervasdoué2010.pdf)
    -   (*facultatif*) [Saint-Arnaud (1999), « L’egalité d’accès aux soins en
        situation de rareté »](/readings/SaintArnaudCh10.pdf)
    -   (*facultatif*) [de Singly (2010), « Le « soin juste » sous contrainte
        économique à l’hôpital »](/readings/deSingly2010.pdf)
    -   [Clarke, « Délais d’attente »](/readings/ClarkeAttente.pdf) ([also
        available in English](/readings/ClarkeWaiting.pdf))
-   6 novembre :
    -   [Dumitru et Leplège (2010), « La course aux brevets dans la médecine
        personnalisée : une étude de cas »](/readings/Dumitru2010.pdf)
    -   [Mary (2021), « Essais cliniques : l’éthique face à
        l’innovation »](/readings/Mary2021.pdf)
    -   (*facultatif*) [Halioua (2010), « Du procès au Code de Nuremberg :
        principes de l’éthique biomédicale »](/readings/Halioua2010.pdf)
-   20 novembre :
    -   [Hirsch (2010), « Les biotechnologies, un nouveau paradigme de la
        recherche »](/readings/Hirsch2010.pdf)
    -   [Mezer et Levin, « Les considérations éthiques liées au diagnostic
        génétique présymptomatique »](/readings/MezerDiagnostic.pdf) ([also
        available in English](/readings/MezerTesting.pdf))
-   27 novembre :
    -   [Hirsch (2018), « CRISPR : lorsque modifier le génome devient
        possible »](/readings/Hirsch2018.pdf)
    -   [Morange (2001), « L’eugénisme aujourd’hui »](/readings/Morange2001.pdf)
    -   [Goffette et Jacobi (2006), « Discours eugéniste d’hier et discours
        “eugéniste” d’aujourd’hui »](/readings/Goffette2006.pdf)
    -   (*facultatif*) [Briard (2010), « Approche éthique de la
        génétique »](/readings/Briard2010.pdf)
    -   [Samuël et Tremblay, « La recherche sur les thérapies géniques et les
        droits des enfants »](/readings/SamuelEnfants.pdf) ([also available in
        English](/readings/SamuelChildren.pdf))
-   4 décembre :
    -   [Brock (1997), “Cloning human beings: an assessment of the ethical
        issues pro and con”)](/readings/Brock1997.pdf)
    -   [Atlan (2002), « De la biologie à l’éthique : Le « clonage »
        thérapeutique »](/readings/Atlan2002.pdf)
    -   (*facultatif*) [Jouneau et Renard (2002), « Cellules souches
        embryonnaires et clonage thérapeutique »](/readings/Jouneau2002.pdf)
    -   [Isasi et Shukairy, « Le clonage humain »](/readings/IsasiClonage.pdf)
        ([also available in English](/readings/IsasiCloning.pdf))
-   11 décembre :
    -   [Flocco (2018), « Points de vue éthiques sur la biologie de synthèse :
        la « marche du progrès » en question »](/readings/Flocco2018.pdf)
    -   [Comité d’éthique de l’Inserm (2020), « La recherche sur les
        organoïdes : quels enjeux éthiques ? »](/readings/InsermOrganoidsFR.pdf)
        ([also available in English](/readings/InsermOrganoids.pdf))
    -   [Rozenbaum (2019), « Organoïdes : quelle place dans la recherche de
        demain ? »](/readings/Rozenbaum2019.pdf)
-   18 décembre :
    -   [Baertschi (2011), « Qu’est-ce qu’une véritable
        amélioration ? »](/readings/Baertschi2011.pdf)
    -   [Hottois (2018), « Bioéthique, technosciences et
        transhumanisme »](/readings/Hottois2018.pdf)
    -   (*facultatif*) [McGee (2020), “Using the therapy and enhancement
        distinction in law and policy”](/readings/McGee2020.pdf)
    -   (*facultatif*) [Goffi (2017),
        « Transhumanisme »](https://encyclo-philo.fr/transhumanisme-a)

--------------------------------------------------------------------------------

## Autres ressources

-   Avis du Comité consultatif de Bioéthique de Belgique
    -   [1 (fr) - Règlement légal de l’euthanasie](/readings/CCBB1fr.pdf)
    -   [1 (en) - Legal regulation on euthanasia](/readings/CCBB1en.pdf)
    -   [18 (fr) - Recherche sur l’embryon humain](/readings/CCBB18fr.pdf)
    -   [18 (en) - Human embryo research](/readings/CCBB18en.pdf)
    -   [24 (fr) - Clonage thérapeutique](/readings/CCBB24fr.pdf)
    -   [33 (fr) - Modifications géniques](/readings/CCBB33fr.pdf)
    -   [33 (en) - Genetic modification](/readings/CCBB33en.pdf)
    -   [36 (fr) - Éthique et sciences humaines](/readings/CCBB36fr.pdf)
    -   [36 (en) - Ethics in human sciences](/readings/CCBB36en.pdf)
    -   [41 (fr) - Consentement éclairé et DNR](/readings/CCBB41fr.pdf)
    -   [41 (en) - Informed consent and DNR](/readings/CCBB41en.pdf)
    -   [47 (fr) - Exceptions d’usage compassionnel et
        d’urgence](/readings/CCBB47fr.pdf)
    -   [47 (en) - Compassionnate use and medical need
        exceptions](/readings/CCBB47en.pdf)
    -   [49 (fr) - Diagnostique préimplantatoire pour porteurs
        sains](/readings/CCBB49fr.pdf)
    -   [49 (en) - Preimplantation diagnosis of healthy
        carriers](/readings/CCBB49en.pdf)

<!--
  removed from to accommodate guest lecture
- 27 octobre :
  - [Le Breton (2010), « Éthique des soins en situation interculturelle à
    l'hôpital »](/readings/LeBreton2010.pdf)
  - _choisir au moins un des suivants :_ - [Ouédraogo (2010), « Approche philosophique de la maladie en Afrique
    noire »](/readings/Ouedraogo2010.pdf) - [Aggoun (2010), « Les musulmans, leur corps, la maladie et la
    mort »](/readings/Aggoun2010.pdf) - [Guo (1995), « Chinese Confucian culture and the medical ethical
    tradition »](/readings/Guo1995.pdf) - [Qiu (1993), « Chinese medical ethics and
    euthanasia »](/readings/Qiu1993.pdf)
-->
