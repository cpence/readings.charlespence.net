---
title: "LFILO2602 : Lecture"
---

# LFILO2602 : Readings / Lecture

<a href="https://charlespence.net/fr/courses/lfilo2602/" class="button button-primary">Général</a>
<a href="https://charlespence.net/fr/courses/lfilo2602/agenda/" class="button button-primary">Agenda</a>
<a href="lfilo2602.php" class="button button-primary">Lecture</a>
<a href="https://charlespence.net/fr/courses/lfilo2602/travail/" class="button button-primary">Travail</a>

<a href="https://charlespence.net/courses/lfilo2602/" class="button button-primary">General</a>
<a href="https://charlespence.net/courses/lfilo2602/schedule/" class="button button-primary">Schedule</a>
<a href="lfilo2602.php" class="button button-primary">Readings</a>
<a href="https://charlespence.net/courses/lfilo2602/paper/" class="button button-primary">Final Paper</a>

---

Toute de la lecture pour ce cours est ci-dessous. / All of the readings for this
course can be found in this list.

- 6 févr: [Carnap, "On the Character of Philosophic
  Problems"](/readings/Carnap1934.pdf)
- 6 févr: [Popper, "Science: Conjectures and
  Refutations"](/readings/Popper1963.pdf)
- 13 févr: [Hanson, "Observation"](/readings/Hanson1958.pdf)
- 13 févr: [Feyerabend, _Against Method_ part 16](/readings/Feyerabend1975.pdf)
- 20 févr-5 mars: [Kuhn, _Structure of Scientific
  Revolutions_](/readings/Kuhn1996Full.pdf)
- 12 mars: [Hempel and Oppenheim, "Studies in the Logic of Explanation"
  (excerpts)](/readings/HempelOppenheim1948.pdf)
- 12 mars: [van Fraassen, "The Pragmatics of
  Explanation"](/readings/vanFraassen1977.pdf)
- 19 mars: [Maxwell, "The Ontological Status of Theoretical
  Entities"](/readings/Maxwell1962.pdf)
- 19 mars: [van Fraassen, "Arguments Concerning Scientific
  Realism"](/readings/vanFraassen1980.pdf)
- 19 mars: [Laudan, "A Confutation of Convergent
  Realism"](/readings/Laudan1981.pdf)
- 26 mars: [Douglas, "Origins of the Value-Free Ideal for
  Science"](/readings/Douglas2009VFI.pdf)
- 26 mars (_facultatif/optional_): [Douglas, "Values in
  Science"](/readings/Douglas2016Handbook.pdf)
- 26 mars (_facultatif/optional_): [Edward Teller's letter to Leo Szilard about
  the atomic bomb](/readings/TellerSzilardLetter.pdf)
- 16 avril (_soit_): [Richardson, "When Gender Criticism Becomes Standard
  Practice"](/readings/Richardson2008.pdf)
- 16 avril (_soit_): [Okruhlik, "Gender and the Biological
  Sciences"](/readings/Okruhlik1994.pdf)
- 23 avril : [Daston and Galison, "The Image of
  Objectivity"](/readings/DastonGalison1992.pdf)
- 30 avril : [Chang, "Epistemic Activities and Systems of
  Practice"](/readings/Chang2014.pdf)
