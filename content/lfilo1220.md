---
title: "LFILO1220 : Lecture"
---

# LFILO1220 : Lecture

<a href="https://charlespence.net/fr/courses/lfilo1220/" class="button button-primary">Général</a>
<a href="https://charlespence.net/fr/courses/lfilo1220/agenda/" class="button button-primary">Agenda</a>
<a href="lfilo1220.php" class="button button-primary">Lecture</a>

--------------------------------------------------------------------------------

Toute de la lecture pour ce cours est ci-dessous.

- 4 février : [Bachelard (1934), « La complexité essentielle de la philosophie
  scientifique »](/readings/Bachelard1934Intro.pdf)
- 5 février : [Kuhn (1983 [1962]), _La structure des révolutions scientifiques_
  (extraits)](/readings/Kuhn1983extr.pdf)
- 11 février : [Shapin (1998 [1996]), _La révolution scientifique_ (introduction
  et chapitre 1)](/readings/Shapin1998.pdf)
- 12 février : [Duhem (1913), « La dynamique des Hellènes après
  Aristote »](/readings/Duhem1913vide.pdf)
- 18 février :
  - [Bonnet-Bidaud (2013), « La science
    chinoise »](/readings/BonnetBidaud2013.pdf)
  - [Weinberg (2013), « L'apport du monde arabe »](/readings/Weinberg2013.pdf)
- 25 février : [Bensaude-Vincent & Stengers (1993), _Histoire de la chimie_
  (extrait)](/readings/BensaudeVincent1993extr.pdf)
- 26 février : [Lamarck (1809), _Philosophie zoologique_
  (extraits)](/readings/Lamarck1809extr.pdf)
- 4 mars : [Darwin (2013 [1859]), _L'origine des espèces_
  (extraits)](/readings/Darwin1859frextr.pdf)
- 5 mars : [Support pour TP 1](/readings/lfilo1220-tp1.pdf)
- 5 mars : [Images pour TP 1](/readings/lfilo1220-tp1-images.pdf)
- 11 mars : [Besnier (2006), « Le statut de l'espace dans la _Critique de la
  raison pure_ de Kant »](/readings/Besnier2006.pdf)
- 12 mars : [Bachelard (1934), « La mécanique
  non-newtonienne »](/readings/Bachelard1934Ch2.pdf)
- 18 mars : **à venir**
- 19 mars : [Feyerabend (1979 [1975]), _Contre la méthode,_ ch.
  16](/readings/Feyerabend1979.pdf)
- 1 avril : [Coutellec (2013), « Une réévaluation du pluralisme
  axiologique »](/readings/Coutellec2013.pdf)
- 2 avril : [Oreskes et Conway (2012 [2010]), _Les marchands de doute_ (ch.
  6)](/readings/OreskesConway2012.pdf)
- 8 avril : [Gould (1997 [1996]), _La mal-mesure de l'homme,_ ch. 5
  (extrait)](/readings/Gould1997.pdf)
- 15 avril : [Galilée (1995 [1638]), _Discours concernant deux sciences nouvelles_
  (extraits)](/readings/Galilee1995.pdf)
- 16 avril : [Varenne (2012), « Modèle et réalité »](/readings/Varenne2012.pdf)
- 7 mai : [Gayon (2005), « De la biologie comme science
  historique »](/readings/Gayon2005.pdf)
- 13 mai : [Debray (2020), « Pseudoscience »](/readings/Debray2020.pdf)
