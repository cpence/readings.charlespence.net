{{ if eq .RelPermalink "/protector.php" }}
{{ "<?php function showForm($error=\"\") { ?>" | safeHTML }}
{{ else }}
{{ if not .IsHome }}
{{ "<?php require_once($_SERVER['DOCUMENT_ROOT'] . '/protector.php'); ?>" | safeHTML }}
{{ end }}
{{ end }}
<!DOCTYPE html>
<html
  lang="{{ default .Site.Language.Lang .Site.LanguageCode }}"
  dir="{{ default "ltr" .Site.Language.LanguageDirection }}"
>
  <head>
    <meta charset="utf-8" />
    <title>{{ .Title }} | {{ .Site.Title -}}</title>

    <meta
      name="description"
      content="{{ default .Site.Params.description .Description }}"
    />
    <meta name="author" content="{{ .Site.Params.author.name }}" />

    <meta name="language" content="{{ .Language.LanguageName }}" />
    {{- range .Translations }}
      <link
        rel="alternate"
        hreflang="{{ default .Language.Lang .Site.LanguageCode }}"
        href="{{ .Permalink }}"
        title="{{ .Title }} | {{ .Site.Title -}}"
      />
    {{- end -}}

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="robots" content="noai, noimageai">

    <link
      rel="stylesheet"
      href="{{ if .Site.Params.dark }}
        {{ "main-dark.css" | relURL }}
      {{ else }}
        {{ "main-light.css" | relURL }}
      {{ end }}"
    />
    <script
      type="text/javascript"
      src="{{ "main.min.js" | relURL }}"
      async
      defer
    ></script>
  </head>
  <body>
    {{ partial "cpence/menu.html" . }}


    <div class="container">
      <div class="row">
        <div class="twelve columns">
          {{- .Content -}}
        </div>
      </div>
    </div>

    {{ partial "cpence/footer.html" . }}
  </body>
</html>
{{ if eq .RelPermalink "/protector.php" }}
{{ `<?php
 }

// Boy, you sure should not attempt to force this cipher if you want to read
// some of these readings, no sir
$PasswordHash = "3750c667d5cd8aecc0a9213b362066e9";

if (isset($_POST['password'])) {
  $pass = $_POST['password'];

  if (md5($pass) != $PasswordHash) {
    showForm("Incorrect password, please try again");
    exit();
  }
} else {
  showForm();
  exit();
}
?>` | safeHTML }}
{{ end }}
