{{ if eq .RelPermalink "/protector.php" }}
{{ "<?php function showForm($error=\"\") { ?>" | safeHTML }}
{{ else }}
{{ if not .IsHome }}
{{ "<?php require_once($_SERVER['DOCUMENT_ROOT'] . '/protector.php'); ?>" | safeHTML }}
{{ end }}
{{ end }}
