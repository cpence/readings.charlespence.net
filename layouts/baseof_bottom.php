{{ if eq .RelPermalink "/protector.php" }}
{{ `<?php
 }

// Boy, you sure should not attempt to force this cipher if you want to read
// some of these readings, no sir
$PasswordHash = "3750c667d5cd8aecc0a9213b362066e9";

if (isset($_POST['password'])) {
  $pass = $_POST['password'];

  if (md5($pass) != $PasswordHash) {
    showForm("Incorrect password, please try again");
    exit();
  }
} else {
  showForm();
  exit();
}
?>` | safeHTML }}
{{ end }}
