# Readings Website

This is a website designed for hosting the readings for all of my classes. It's
really nothing more than some basic password-protection (in PHP, because my
vhost provider has PHP running all the time) and index pages for the reading
lists.

Of course, this isn't great security, but it suffices to claim to any relevant
authorities that I've done *something* to try to prevent others from accessing
copyrighted material that they shouldn't have had access to.

## License

Anything here that's my own contribution is released under CC-BY 4.0; see the
LICENSE file.
